package matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    private static String pathFileMatrix1 = "/matrixProblemA/matrixProblemSet1.txt";
    private static int numberOfRow1 = 200;
    private static int numberOfCol1 = 200;

    private static String pathFileMatrix2 = "/matrixProblemA/matrixProblemSet2.txt";
    private static int numberOfRow2 = 200;
    private static int numberOfCol2 = 200;

    public static void main(String[] args) throws
            IOException, InvalidMatrixSizeForMultiplicationException {

        //Convert into array
        double[][] firstMatrix = convertInputFileToMatrix(new File("src").getAbsolutePath() + pathFileMatrix1, numberOfRow1, numberOfCol1);
        double[][] secondMatrix = convertInputFileToMatrix(new File("src").getAbsolutePath() + pathFileMatrix2, numberOfRow2, numberOfCol2);

        //Example usage of basic multiplication algorithm.
        long totalMilis = System.currentTimeMillis();
        double[][] multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);
        totalMilis = System.currentTimeMillis() - totalMilis;
        System.out.println("Basic Multiplication Complete in " + totalMilis + " milisecond");

        //Example usage of strassen multiplication algorithm.
        totalMilis = System.currentTimeMillis();
        double[][] strassenMultiplicationResult =
                MatrixOperation.strassenMatrixMultiForNonSquareMatrix(firstMatrix, secondMatrix);
        totalMilis = System.currentTimeMillis() - totalMilis;
        System.out.println("Strassen Multiplication Complete in " + totalMilis + " milisecond");

    }

    /**
     * Converting a file input into an 2 dimensional array of double that represent a matrix.
     * @param pathFile is a path to file input.
     * @param numberOfRow the number of row inside the matrix.
     * @param numberOfCol the number of col inside the matrix.
     * @return 2 dimensional array of double representing matrix.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static double[][] convertInputFileToMatrix(String pathFile, int numberOfRow, int numberOfCol)
            throws IOException {
        File matrixFile = new File(pathFile);
        FileReader fileReader = new FileReader(matrixFile);
        double[][] matrix = new double[numberOfRow][numberOfCol];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;

        for (int i = 0; i < numberOfRow; i++) {
            currentLine = bufferedReader.readLine();
            matrix[i] = sequenceIntoArray(currentLine, numberOfCol);
        }
        return matrix;
    }

    /**
     * Converting a row of sequence of double into an array.
     * @param currentLine sequence of double from input representing a row from matrix.
     * @return array of double representing a row from matrix.
     */
    private static double[] sequenceIntoArray(String currentLine, int numberOfCol) {
        String[] arrInput = currentLine.split(" ");
        double[] arrInteger = new double[numberOfCol];
        for (int index = 0; index < numberOfCol; index++) {
            arrInteger[index] = Double.parseDouble(arrInput[index]);
        }
        return arrInteger;
    }
}
