package sorting;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {
    private static int[] sortedArr;
    private static int[] unsortedArr;

    private static String file = "sortingProblem.txt";
    private static int numberOfItemToBeSorted = 50000;

    @Before
    public void setUp() throws IOException {
        File sortingProblemFile = new File(new File("src").getAbsolutePath() + "/" + file);
        FileReader fileReader = new FileReader(sortingProblemFile);
        unsortedArr = new int[numberOfItemToBeSorted];
        sortedArr = new int[numberOfItemToBeSorted];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            unsortedArr[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }


        for (int i = 0; i < numberOfItemToBeSorted; i++) {
            sortedArr[i] = unsortedArr[i];
        }

        Arrays.sort(sortedArr);
    }

    @Test
    public void fastSortTest() {
        Sorter.fastSort(unsortedArr);
        for (int i = 0; i < numberOfItemToBeSorted; i++) {
            assertTrue(unsortedArr[i] == sortedArr[i]);
        }
    }

    @Test
    public void fastSearchTest() {
        assertTrue(Finder.fastSearch(sortedArr, 40738) == 40738);
    }
}
