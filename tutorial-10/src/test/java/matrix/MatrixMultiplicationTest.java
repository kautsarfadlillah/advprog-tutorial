package matrix;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

public class MatrixMultiplicationTest {
    private static String genericPath = new File("src").getAbsolutePath();
    private static String pathFileMatrix1 = genericPath
                                                + "/matrixProblemB/matrix10rows50columns.txt";
    private static String pathFileMatrix2 = genericPath
                                                + "/matrixProblemB/matrix50row10column.txt";
    private static String pathFileMatrix3 = genericPath
                                                + "/matrixProblemA/matrixProblemTest1.txt";
    private static String pathFileMatrix4 = genericPath
                                                + "/matrixProblemA/matrixProblemTest2.txt";
    private static String pathFileMatrix5 = genericPath
                                                + "/matrixProblemA/matrixResult.txt";

    private static final double EPS = 1e-9;

    private static double[][] convertInputFileToMatrix(
            String pathFile, int numberOfRow, int numberOfCol)
                throws IOException {
        File matrixFile = new File(pathFile);
        FileReader fileReader = new FileReader(matrixFile);
        double[][] matrix = new double[numberOfRow][numberOfCol];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;

        for (int i = 0; i < numberOfRow; i++) {
            currentLine = bufferedReader.readLine();
            matrix[i] = sequenceIntoArray(currentLine, numberOfCol);
        }
        return matrix;
    }

    private static double[] sequenceIntoArray(String currentLine, int numberOfCol) {
        String[] arrInput = currentLine.split(" ");
        double[] arrInteger = new double[numberOfCol];
        for (int index = 0; index < numberOfCol; index++) {
            arrInteger[index] = Double.parseDouble(arrInput[index]);
        }
        return arrInteger;
    }

    @Test
    public void nonSquareMatrixMultiplicationTest()
            throws IOException, InvalidMatrixSizeForMultiplicationException {
        int matrixRow1 = 10;
        int matrixCol1 = 50;
        int matrixRow2 = 50;
        int matrixCol2 = 10;

        double[][] firstMatrix = convertInputFileToMatrix(pathFileMatrix1, matrixRow1, matrixCol1);
        double[][] secondMatrix = convertInputFileToMatrix(pathFileMatrix2, matrixRow2, matrixCol2);

        double[][] multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);

        double[][] strassenMultiplicationResult =
                MatrixOperation.strassenMatrixMultiForNonSquareMatrix(firstMatrix, secondMatrix);

        for (int i = 0; i < matrixRow1; i++) {
            for (int j = 0; j < matrixCol2; j++) {
                assertTrue(multiplicationResult[i][j] == strassenMultiplicationResult[i][j]);
            }
        }
    }

    @Test
    public void squareMatrixMultiplicationTest()
            throws IOException, InvalidMatrixSizeForMultiplicationException {
        int matrixRow1 = 50;
        int matrixCol1 = 50;
        int matrixRow2 = 50;
        int matrixCol2 = 50;

        double[][] firstMatrix = convertInputFileToMatrix(pathFileMatrix3, matrixRow1, matrixCol1);
        double[][] secondMatrix = convertInputFileToMatrix(pathFileMatrix4, matrixRow2, matrixCol2);
        double[][] resultMatrix = convertInputFileToMatrix(pathFileMatrix5, matrixRow2, matrixCol2);

        double[][] multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);

        double[][] strassenMultiplicationResult =
                MatrixOperation.strassenMatrixMultiForNonSquareMatrix(firstMatrix, secondMatrix);

        for (int i = 0; i < matrixRow1; i++) {
            for (int j = 0; j < matrixCol2; j++) {
                assertTrue(Math.abs(multiplicationResult[i][j] - resultMatrix[i][j]) < EPS);
            }
        }

        for (int i = 0; i < matrixRow1; i++) {
            for (int j = 0; j < matrixCol2; j++) {
                assertTrue(Math.abs(strassenMultiplicationResult[i][j] - resultMatrix[i][j]) < EPS);
            }
        }
    }
}
