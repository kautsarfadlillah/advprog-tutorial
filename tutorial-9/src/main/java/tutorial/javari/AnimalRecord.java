package tutorial.javari;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class AnimalRecord {

    private AtomicInteger counter = new AtomicInteger();
    private ArrayList<Animal> animals = new ArrayList<>();

    public Animal addAnimal(String type, String name, String gender, double length,
                            double weight, String condition) {
        Animal animal = new Animal(counter.incrementAndGet(),
                type, name,Gender.parseGender(gender),
                length, weight, Condition.parseCondition(condition));
        animals.add(animal);
        
        return animal;
    }

    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    public Animal getAnimal(int id) {
        List<Animal> selectedAnimal = animals.stream()
                .filter(animal -> animal.getId() == id)
                .collect(Collectors.toList());

        if (selectedAnimal.size() == 1) {
            return selectedAnimal.get(0);
        }

        return null;
    }

    public Animal deleteAnimal(int id) {
        List<Animal> deletedAnimal = animals.stream()
                .filter(animal -> animal.getId() == id)
                .collect(Collectors.toList());

        if (deletedAnimal.size() == 1) {
            Animal deleted = deletedAnimal.get(0);
            animals.remove(deleted);
            return deleted;
        }

        return null;
    }
}
