package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employee) {
        employeesList.add(employee);
    }

    public double getNetSalaries() {
        return employeesList.stream().mapToDouble(employee -> employee.getSalary()).sum();
    }

    public List<Employees> getAllEmployees() {
        return this.employeesList;
    }
}
