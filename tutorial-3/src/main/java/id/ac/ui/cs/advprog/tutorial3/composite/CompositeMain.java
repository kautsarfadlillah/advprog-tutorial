package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;

public class CompositeMain {

    public static void main(String[] args) {
        Company company = new Company();
        Ceo ceo = new Ceo("CEO", 2000000);
        Cto cto = new Cto("CEO", 600000);

        company.addEmployee(ceo);
        System.out.println("Add CEO to the Company");
        company.addEmployee(cto);
        System.out.println("Add CTO to the Company");

        System.out.printf("Total salaries of the company = %.2f\n", company.getNetSalaries());

        Company otherCompany = new Company();
        Ceo ceo2 = new Ceo("CEO2", 2000000);

        //Must be error
        Cto cto2 = new Cto("CTO2", 25000);

        otherCompany.addEmployee(ceo2);
        System.out.println("Add CEO to other company");
        otherCompany.addEmployee(cto2);
        System.out.println("Add CTO to other company");

        System.out.printf("Total salaries of other company = %.2f\n", otherCompany.getNetSalaries());
    }

}
