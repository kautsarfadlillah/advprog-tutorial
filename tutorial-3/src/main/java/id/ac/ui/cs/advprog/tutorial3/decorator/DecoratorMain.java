package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {

    public static void printFoodDescription(Food food) {
        System.out.printf("Food description: %s \nCost: %.2f\n", food.getDescription(), food.cost());
    }

    public static void main(String[] args) {
        Food myBurger = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        printFoodDescription(myBurger);

        myBurger = FillingDecorator.CHEESE.addFillingToBread(myBurger);
        printFoodDescription(myBurger);

        myBurger = FillingDecorator.CHICKEN_MEAT.addFillingToBread(myBurger);
        printFoodDescription(myBurger);

        myBurger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(myBurger);
        printFoodDescription(myBurger);

        myBurger = FillingDecorator.CUCUMBER.addFillingToBread(myBurger);
        printFoodDescription(myBurger);
    }

}
