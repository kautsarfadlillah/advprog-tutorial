import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomerTest {

    private Customer customer;
    private Movie movie;
    private Movie movie2;
    private Movie movie3;

    private Rental rent;
    private Rental rent2;
    private Rental rent3;

    @Before
    public void setUp() {
        customer = new Customer("Alice");

        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Tebaatusasula", Movie.NEW_RELEASE);
        movie3 = new Movie("Bad Black", Movie.CHILDREN);

        rent = new Rental(movie, 3);
        rent2 = new Rental(movie2, 2);
        rent3 = new Rental(movie3, 10);

    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 21.5"));
        assertTrue(result.contains("4 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5<br>"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 21.5<br>"));
        assertTrue(result.contains("4 frequent renter points"));
    }

}